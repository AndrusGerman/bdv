package bdv

import rest "gopkg.in/resty.v1"

// LoginByURL login in bdv by url login
func (ctx *AuthResponse) LoginByURL(url string) error {
	_, err := ctx.request(ctx).Post(url)
	return err
}

// GetAccountTercero obtener la cuentas afilidas de terceros
func (ctx *AuthResponse) GetAccountTercero() *[]AccountTercero {
	var resp = new([]AccountTercero)
	ctx.request(resp).Get("https://bdvenlinea.banvenez.com/registrosafiliaciones/cuentasRegistradasBdv/")
	return resp
}

// GetAccountTerceroOtherBanks obtener la cuentas afilidas de terceros de otros bancos
func (ctx *AuthResponse) GetAccountTerceroOtherBanks() *[]AccountTerceroOtherBank {
	var resp = new([]AccountTerceroOtherBank)
	ctx.request(resp).Get("https://bdvenlinea.banvenez.com/registrosafiliaciones/cuentasOtrosBancos/")
	return resp
}

// GetBanks para PagoClave
func (ctx *AuthResponse) GetBanks() *BanksResponse {
	var resp = new(BanksResponse)
	ctx.request(resp).Get("ttps://bdvenlinea.banvenez.com/datosgenerales/listadoBancos/p2p")
	return resp
}

// GetMyAccounts obtener las cuentas de mi banco
func (ctx *AuthResponse) GetMyAccounts() *[]DatosCuentaResponse {
	var resp = new([]DatosCuentaResponse)
	ctx.request(resp).Get("https://bdvenlinea.banvenez.com/consultasaldocuenta/consultaSaldoCuenta")
	return resp
}

// GetOperations ver todas las operaciones
func (ctx *AuthResponse) GetOperations(days int) *OperacionResponse {
	// Set Data
	type Data struct {
		Orden      string      `json:"orden"`
		Ascdesc    string      `json:"ascdesc"`
		TpDcto     string      `json:"tpDcto"`
		NroDcto    string      `json:"nroDcto"`
		TpTrans    interface{} `json:"tpTrans"`
		FechaDesde string      `json:"fechaDesde"`
		FechaHasta string      `json:"fechaHasta"`
		DatosReg   string      `json:"datosReg"`
		CodRet     string      `json:"codRet"`
		DeCodret   string      `json:"deCodret"`
	}
	var data = Data{
		Ascdesc:    "DESC",
		Orden:      "FECHA",
		TpTrans:    nil,
		FechaDesde: GetTimeInFormat(days),
		FechaHasta: GetTimeInFormat(0),
	}
	var resp = new(OperacionResponse)
	// Request
	ctx.request(resp).SetBody(data).Post("https://bdvenlinea.banvenez.com/historicooperaciones/historicoOperaciones")
	return resp
}

func (ctx *AuthResponse) request(result interface{}) *rest.Request {
	client := rest.New()
	return client.NewRequest().SetAuthToken(ctx.AccessToken).SetResult(result)
}
