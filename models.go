package bdv

// AuthResponse modelo para el login
type AuthResponse struct {
	AccessToken    string `json:"access_token"`
	TokenType      string `json:"token_type"`
	RefreshToken   string `json:"refresh_token"`
	ExpiresIn      int    `json:"expires_in"`
	Scope          string `json:"scope"`
	LastName       string `json:"lastName"`
	IDType         string `json:"idType"`
	Gender         string `json:"gender"`
	Crypt          string `json:"crypt"`
	IP             string `json:"ip"`
	LastConnection string `json:"lastConnection"`
	Factor3        string `json:"factor3"`
	FechaNac       string `json:"fechaNac"`
	Cord2          string `json:"cord2"`
	Cord1          string `json:"cord1"`
	Phone          string `json:"phone"`
	Name           string `json:"name"`
	SocialLevel    string `json:"socialLevel"`
	ID             string `json:"id"`
	Jti            string `json:"jti"`
}

// DatosCuentaResponse modelo para las cuentas
type DatosCuentaResponse struct {
	Tipo               string `json:"tipo"`
	Cuenta             string `json:"cuenta"`
	Divisa             string `json:"divisa"`
	SaldoDisponible    string `json:"saldoDisponible"`
	DisponibleTotal    string `json:"disponibleTotal"`
	SaldoDispuesto     string `json:"saldoDispuesto"`
	SaldoRetenido      string `json:"saldoRetenido"`
	SaldoRetenidoPrec  string `json:"saldoRetenidoPrec"`
	SaldoRetenidoHoy   string `json:"saldoRetenidoHoy"`
	DisponibleRetenido string `json:"disponibleRetenido"`
	Excedido           string `json:"excedido"`
	CupoCanje          string `json:"cupoCanje"`
	CupoRemesas        string `json:"cupoRemesas"`
	SaldoCanje         string `json:"saldoCanje"`
	SaldoRemesas       string `json:"saldoRemesas"`
	Limite             string `json:"limite"`
	SaldoRetenidoLiq   string `json:"saldoRetenidoLiq"`
	DisponibleConjunto string `json:"disponibleConjunto"`
}

// OperacionResponse modelo para las operaciones
type OperacionResponse []struct {
	Operacion      string `json:"operacion"`
	Transaction    string `json:"transaccion"`
	Description    string `json:"descripcion"`
	CedulaOtro     string `json:"cedula_otro"`
	InstrumentoOri string `json:"instrumento_ori"`
	InstrumentoDes string `json:"instrumento_des"`
	Concepto       string `json:"concepto"`
	Beneficiario   string `json:"beneficiario"`
	Fecha          string `json:"fecha"`
	Hora           string `json:"hora"`
	Monto          string `json:"monto"`
}

// AccountsMovements modelo para los movimientos de las cuentas
type AccountsMovements []struct {
	Fecha               string `json:"fecha"`
	Referencia          string `json:"referencia"`
	Description         string `json:"descripcion"`
	Debito              string `json:"debito"`
	Credito             string `json:"credito"`
	Saldo               string `json:"saldo"`
	TipoMovimiento      string `json:"tipoMovimiento"`
	IndicadorCargoAbono string `json:"indicadorCargoAbono"`
	Importe             string `json:"importe"`
}

// AccountTercero modelo para las cuentas de terceros
type AccountTercero struct {
	Cuenta        string `json:"cuenta"`
	Tipo          string `json:"tipo"`
	Beneficiario  string `json:"beneficiario"`
	TipoDocumento string `json:"tipoDocumento"`
	Documento     string `json:"documento"`
	Alias         string `json:"alias"`
	Correo        string `json:"correo"`
	Instrumento   string `json:"instrumento"`
	CodigoBanco   string `json:"codigoBanco"`
}

// AccountTerceroOtherBank modelo para la respuesta de las cuentas de otros bancos de tercero
type AccountTerceroOtherBank struct {
	Banco         string `json:"banco"`
	CodigoBanco   string `json:"codigoBanco"`
	Instrumento   string `json:"instrumento"`
	TipoDocumento string `json:"tipoDocumento"`
	NroDocumento  string `json:"nroDocumento"`
	Beneficiario  string `json:"beneficiario"`
	Tipo          string `json:"tipo"`
	Description   string `json:"descripcion"`
	Alias         string `json:"alias"`
	Correo        string `json:"correo"`
	Documento     string `json:"documento"`
}

// BanksResponse modelo para la respuesta de los bancos
type BanksResponse []struct {
	CodigoBanco string `json:"codigoBanco"`
	NombreBanco string `json:"nombreBanco"`
}

// TranferenciaTerceroResponse modelo para repuesta de una transacion
type TranferenciaTerceroResponse struct {
	CodRespuesta   string `json:"codRespuesta"`
	Referencia     string `json:"referencia"`
	NroComprobante string `json:"nroComprobante"`
}
