package bdv

import "time"

// TranferirATercero trainfiere a una persona de otro banco
func TranferirATercero(auth *AuthResponse, miCuenta *DatosCuentaResponse, suCuenta *AccountTercero, importe int) (*TranferenciaTerceroResponse, error) {
	type Data struct {
		CuentasCargo  string `json:"cuentasCargo"`
		CuentaAbono   string `json:"cuentaAbono"`
		Importe       int    `json:"importe"`
		NacionDestino string `json:"nacionDestino"`
		CedulaDestino string `json:"cedulaDestino"`
		Modalidad     string `json:"modalidad"`
		NombreDestino string `json:"nombreDestino"`
	}
	var data = Data{
		CuentasCargo:  miCuenta.Cuenta,
		CuentaAbono:   suCuenta.Cuenta,
		NacionDestino: "V",
		CedulaDestino: suCuenta.Documento,
		Modalidad:     "0",
		NombreDestino: suCuenta.Alias,
		Importe:       importe,
	}
	resp := new(TranferenciaTerceroResponse)
	_, er := auth.request(resp).SetBody(data).Post("https://bdvenlinea.banvenez.com/transferencias/cuentasTerceros")
	return resp, er
}

// GetTimeInFormat obtener tiempo en un formato valido para bdv
func GetTimeInFormat(days int) string {
	now := time.Now()
	daysMenos := -(time.Duration(24*days) * time.Hour)
	return now.Add(daysMenos).Format("02-01-2006")
}
