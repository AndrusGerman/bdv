package bdv

// Tranferir le tranfiere la cantidad determinada a esta cuenta
func (ctx *AccountTercero) Tranferir(auth *AuthResponse, miCuenta *DatosCuentaResponse, importe int) (*TranferenciaTerceroResponse, error) {
	return TranferirATercero(auth, miCuenta, ctx, importe)
}
