package bdv

// TranferirATercero trainfiere a un tercero
func (ctx *DatosCuentaResponse) TranferirATercero(auth *AuthResponse, suCuenta *AccountTercero, importe int) (*TranferenciaTerceroResponse, error) {
	return TranferirATercero(auth, ctx, suCuenta, importe)
}

// GetMovements obtener los movimientos de una cuenta
func (ctx *DatosCuentaResponse) GetMovements(auth *AuthResponse, days int) *AccountsMovements {
	// Set Data
	type Data struct {
		Commission         string `json:"comision"`
		NumeroCuenta       string `json:"numeroCuenta"`
		Divisa             string `json:"divisa"`
		FechaDesde         string `json:"fechaDesde"`
		FechaHasta         string `json:"fechaHasta"`
		MovimientoInicial  string `json:"movimientoInicial"`
		MovimientoFinal    string `json:"movimientoFinal"`
		RegistroARecuperar string `json:"registroARecuperar"`
		TipoDeRegistro     string `json:"tipoDeRegistro"`
	}
	var data = Data{
		FechaDesde:        GetTimeInFormat(days),
		FechaHasta:        GetTimeInFormat(0),
		NumeroCuenta:      ctx.Cuenta,
		MovimientoFinal:   "0",
		MovimientoInicial: "0",
		Commission:        "false",
	}
	var resp = new(AccountsMovements)
	// Request
	auth.request(resp).SetBody(data).Post("https://bdvenlinea.banvenez.com/movimientoscuentaenlinea/movimientosCuentaEnLinea")
	return resp
}
